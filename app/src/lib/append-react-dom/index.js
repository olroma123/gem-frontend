var React = require('../../../node_modules/react');
var ReactDOM = require('../../../node_modules/react-dom');

module.exports = function appendReactDOM(Component, el, props, done) {
	if (typeof props === 'function') {
		done = props;
		props = null;
	}

	if (!props) {
		props = {};
	}

	if (el.length) {
		el = Array.prototype.slice.call(el);
	} else {
		el = [el];
	}

	el.forEach(function (dom) {
		let div = document.createElement('div');
		ReactDOM.render(React.createElement(Component, props, null), div, function () {
			dom.appendChild(ReactDOM.findDOMNode(this));
			typeof done === 'function' && done();
		});
	});
}

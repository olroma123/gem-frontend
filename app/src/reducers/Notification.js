const initialState = {
  messages: 0,
  friends: 0
};

export default function notificationReducer(state = initialState, action) {
  if (action.type === 'NOTIFICATION_UPDATE')
    return action.payload;
  else
    return state;
}

export default class SlidemenuAction {
  static hideMenu() {
    return {type: 'HIDE_MENU'}
  }

  static unhideMenu() {
    return {type: 'UNHIDE_MENU'}
  }
}

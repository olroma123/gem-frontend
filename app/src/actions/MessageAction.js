export default class MessageAction {
  static setMessage(message) {
    return {
      type: 'SET_MESSAGE',
      payload: message
    }
  }
}

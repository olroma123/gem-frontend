import React from 'react';
import LegendItem from '../LegendItem';
import Container from '../Container';
import Row from '../Row';
import Col from '../Col';

import "./Legend.scss";

export default class Legend extends React.Component {
  render() {
    return (
      <div className="legend">
        <h2 className="legend__header">Архив</h2>
        <Container>
           <Row>
              <Col style={{paddingLeft: '0'}} xs="12" sm="6">
               <LegendItem text="Онлайн-консультация" color="orange" />
               <LegendItem text="Лабороторные обследования" color="green"/>
              </Col>

              <Col xs="12" sm="6">
                <LegendItem text="Осмотр специалиста" color="blue"/>
                <LegendItem text="Санаторно-курортное лечение" color="purple"/>
              </Col>
           </Row>
        </Container>
      </div>
    );
  }
}

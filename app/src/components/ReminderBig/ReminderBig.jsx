import React from 'react';
import Row from '../Row';
import Col from '../Col';
import './ReminderBig.scss';

class ReminderBig extends React.Component {
  state = {
    type: this.props.type,
    date: this.props.date
  };

  componentWillMount() {
    if (this.props.date.includes('.')) {
      let rawDate = this.props.date.split('.');

      if (rawDate.length === 0 || rawDate.length > 3)
        throw new Error('Wrong date format on remind-big!');

      let monthName = new Date().getMonthNameByDay(rawDate[1]);
      this.setState({date: `${rawDate[0]} ${monthName}`});
    }
  }

  render() {
    const { type } = this.state;
    let typeClassName = 'reminder-big__type--primary';
    if (type === 'профилактика' || type === 'Онлайн-консультация') typeClassName = 'reminder-big__type--warning';
    else if (type === 'Санаторно-курортное лечение') typeClassName = 'reminder-big__type--purple';
    else if (type === 'Осмотр специалиста') typeClassName = 'reminder-big__type--blue';
    else if (this.state.type === 'день рождения') typeClassName = 'reminder-big__type--remind';

    return (
      <li className={"reminder-big "+this.props.className}>
        <Row>
          <Col xs="9">
            <div className={"reminder-big__type " + typeClassName}>{this.props.type}</div>
            <div className="reminder-big__title">{this.props.title}</div>
          </Col>

          <Col xs="3">
            <div className="reminder-big__time">{this.props.time}</div>
            <div className="reminder-big__date">{this.state.date}</div>
          </Col>
        </Row>
      </li>
    );
  }
}

export default ReminderBig;

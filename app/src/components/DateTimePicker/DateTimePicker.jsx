import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'moment/locale/ru';
import './DateTimePicker.scss';
moment.locale('ru');

export default class DateTimePicker extends React.Component {
  state = {
    startDate: this.props.startDate || moment()
  };

  handleChange = (date) => {
    this.setState({
      startDate: date
    });
  };

  componentDidMount() {
    let input = document.querySelectorAll('div.react-datepicker__input-container');
    let calendarIcon = document.createElement('span');
    calendarIcon.classList.add('fa');
    calendarIcon.classList.add('fa-calendar');
    calendarIcon.classList.add('dateTimePicker__icon');
    input.forEach(el => {
      el.appendChild(calendarIcon);
    });
  }

  render() {
    let datePicket;
    if (this.props.showTimeSelect)
      datePicket = (<DatePicker id={this.props.id || "DatePick"} showTimeSelect timeFormat="HH:mm" dateFormat="HH:mm   DD.MM.YYYY" selected={this.state.startDate} onChange={this.handleChange}/>);
    else
      datePicket = (<DatePicker id={this.props.id || "DatePick"} dateFormat="DD.MM.YYYY" selected={this.state.startDate} onChange={this.handleChange}/>);
    return (
      <div className={'dateTimePicker ' + (this.props.className || '')}>
        <div className="dateTimePicker__title">{this.props.title}</div>
        {datePicket}
      </div>
    );
  }
}

import React, {Component} from 'react';
import './ProfileDocument.scss';
import Card from "../Card";

export default class ProfileDocument extends Component {
  render() {
    return (
      <Card className={'profile-edit__documents ' + (this.props.className || '')}>
        {this.props.children}
      </Card>
    );
  }
}

import React from 'react';
import InputSimple from '../InputSimple';
import Form from '../Form';

import "./AddRemind.scss";

export default class AddRemind extends React.Component {
  render() {
    return (
      <div>
      	<Form className="AddRemind">
          <InputSimple id="about" value="" labelText="О себе" cols="30" rows="5" isMultiline={true}/>
        </Form>
      </div>
    );
  }
}

import React from 'react';
import {Icon} from 'react-fa';
import { Link } from 'react-router-dom';
import './NavItem.scss';

class NavItem extends React.Component {
  constructor() {
    super(...arguments);
    this.state = {
      showDot: this.props.dot || false,
    }
  }

  render() {
    let dot;
    if (this.state.showDot)
      dot = (<Icon name="circle"/>);

    return (
      <li onClick={this.props.onClick} className={(this.props.isInline ? "header__nav-item" : "") + " nav-item " + (this.props.className || '')}>
        <Link to={this.props.link || '#'}>{this.props.text}</Link>
        {dot}
      </li>
    );
  }
}

export default NavItem;

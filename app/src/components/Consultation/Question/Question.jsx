import React, {Component} from 'react';
import {connect} from 'react-redux';
import Card from '../../Card';
import AvatarCircle from '../../AvatarCircle';
import {Icon} from 'react-fa';
import QuestionComment from '../QuestionComment';
import './Question.scss';
import moment from 'moment';
import { Link } from 'react-router-dom';
import Button from '../../Button';
import InputSimple from '../../InputSimple';
const SlideMenuAction = require('../../../actions/SlidemenuAction.js');

class Question extends Component {
  state = {
    question: {},
    isAPILoaded: false,
    canWrite: false
  };

  componentDidMount() {
    let questionId = require('../../../lib/query-string').parse(window.location.search).id;
    this._getQuestionDetails(questionId);
    this._checkForUserAccessToSendMessage(questionId);
  }

  _getQuestionDetails = (id) => {
    fetch(`${process.env.API_URL}/specialist/question/${id}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      if (response.status === 500 || response.status === 404) {
        console.log('Сервер не отвечает');
        return;
      }

      if (response.status === 204) {
        this.setState({question: [], isAPILoaded: true});
        return;
      }

      response.json().then((data) => {
        this.setState({question: response.status === 200 ? data.data : data.message.message, isAPILoaded: true});
      });
    }).catch((response) => {});
  };

  _checkForUserAccessToSendMessage = (id) => {
    fetch(`${process.env.API_URL}/specialist/question/canWrite/${this.props.user.info._id}?questionId=${id}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      if (response.status === 500 || response.status === 404) {
        console.log('Сервер не отвечает');
        return;
      }

      this.setState({canWrite: response.status === 200});
    }).catch((response) => {});
  };

  addComment = () => {
    let message = document.getElementById('comment').value.trim();
    if (!message || message === '') return;
    fetch(`${process.env.API_URL}/specialist/question/${this.props.user.info._id}?questionId=${this.state.question._id}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: `message=${document.getElementById('comment').value}`
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        console.log('Сервер не отвечает');
        return;
      }

      if (response.status === 200) {
        document.getElementById('comment').value = '';
        this._getQuestionDetails(this.state.question._id);
      }
      else {
        response.json().then(data => {
          console.log(data);
        });
      }
    });
  };

  _closeQuestion = () => {
    fetch(`${process.env.API_URL}/specialist/question/${this.props.user.info._id}/close?questionId=${this.state.question._id}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: `message=${document.getElementById('comment').value}`
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        console.log('Сервер не отвечает');
        return;
      }

      if (response.status === 200) window.location.href = `/questions?theme=${this.state.question.themeId}`;
    });
  };

  render() {
    const getQuestion = () => {
      let diff = moment(new Date()).diff(moment(new Date(this.state.question.dateCreated)), 'days');
      let date = moment(new Date(this.state.question.dateCreated)).format('DD.MM.YYYY');

      return (
        <div>
          <Link className='question-details__back' to={`/questions?theme=${this.state.question.themeId}`}>
            <Icon name='arrow-left'/> Перейти к списку вопросов
          </Link>
          <div className="question-details__header">
            <AvatarCircle img={this.state.question.owner.avatar}/>
            <div className="question-details__title">{this.state.question.text}</div>
          </div>

          <div className="question-details__info">
            <Link to={`/id/${this.state.question.owner.id}`} className="question-details__user">{this.state.question.owner.fullName}</Link>
            <div className="question-details__date">Вопрос открыт: {diff < 2 ? moment(new Date().fromNow(new Date(this.state.question.dateCreated))).fromNow() : date}</div>
            {
              this.state.question.owner.id === this.props.user.info.id && !this.state.question.isClosed ?
                <Button onClick={this._closeQuestion} text='Закрыть вопрос' className='button--primary-small-round question-details__close'/> : null
            }
          </div>

          <div className="question-details__delimiter">
            Всего ответов: {this.state.question.comments.length}
          </div>

          <div className="question-details__comments">
            {this.state.question.comments.map((comment, index) => {
              return (<QuestionComment key={index} comment={comment}/>)
            })}
          </div>
          {
            this.state.canWrite ?
              <div className='question-details__comments-add'>
                <InputSimple className='comment-add' id="comment" labelText="Сообщение" cols="20" rows="5" isMultiline={true}/>
                <Button onClick={this.addComment} text='Отправить сообщение' className='button--primary-small-round'/>
              </div>
              : null
          }
        </div>
      )
    };

    return (
      <Card cardShadow='no-shadow' className='question-details'>
        {this.state.isAPILoaded ? getQuestion() : null}
      </Card>
    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu,
    user: state.user
  }),

  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    },
  })
)(Question);

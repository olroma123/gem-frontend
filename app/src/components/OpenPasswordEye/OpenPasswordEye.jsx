import React from 'react';
import {Icon} from 'react-fa';
import './OpenPasswordEye.scss';

class OpenPasswordEye extends React.Component {
  constructor() {
    super(...arguments);
    this.state = {
      passwordVisible: false,
      iconName: 'eye-slash'
    };

    this.togglePasswordVisibility = this.togglePasswordVisibility.bind(this);
  }

  togglePasswordVisibility() {
    document.getElementById(this.props.id).type = this.state.passwordVisible ? 'password' : 'text';
    this.setState({
      iconName: this.state.passwordVisible ? 'eye-slash' : 'eye',
      passwordVisible: !this.state.passwordVisible
    });
  }

  render() {
    return (
      <Icon onClick={this.togglePasswordVisibility} className='input__open-password' name={this.state.iconName}/>
    )
  }
}

export default OpenPasswordEye;
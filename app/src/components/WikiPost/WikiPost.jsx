import React from 'react';
import './WikiPost.scss';
import Button from "../Button/Button";
import {connect} from "react-redux";
class WikiPost extends React.Component {
  state = {
    structure: []
  };

  RemovePost = (id) => {
    fetch(`${process.env.API_URL}/lib/${this.props.user.info._id}?id=${id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      }
    }).then(response => {
      document.getElementsByClassName('wiki__content')[0].innerHTML = '<span> </span>';
      this.props.onUpdate && this.props.onUpdate();
    });
  };

  render() {
    return (
      <div className={"WikiPost"}>
        <div  className={"WikiPost__Left"}>
          <h3 className={"WikiPost__header"}>{this.props.dat.name}</h3>
          <p className={"WikiPost__text"}>{this.props.dat.text}</p>
        </div>

        <div className={"WikiPost__block"}>
          <img alt='wiki' src={this.props.dat.img} className={"WikiPost__image"}/>

          {
            this.props.isAdmin ?
              <Button onClick={() => this.RemovePost(this.props.dat._id)} text={'Удалить запись'} className='button--orange-ghost Struct__add__Directory__button' />
              : null
          }
        </div>
      </div>
    );
  }
}

export default connect (
  state => ({
    user: state.user
  }))(WikiPost);
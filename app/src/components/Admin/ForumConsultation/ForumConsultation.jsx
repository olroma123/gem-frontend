import React, {Component} from 'react';
import {connect} from 'react-redux';
import SortableTree, { changeNodeAtPath, addNodeUnderParent, removeNodeAtPath } from 'react-sortable-tree';
import Modal from 'react-modal';
import {Icon} from 'react-fa';
import Button from '../../Button';
import '../../../Styles/components/sortable-tree.scss';
import './ForumConsultation.scss';
import InputSimple from '../../InputSimple';
import Card from '../../Card';
const Loader = require('react-loaders').Loader;

class ForumConsultation extends Component {
  state = {
    modalIsOpen: false,
    isAPILoaded: false,
    structure: [],
    typeOpenModal:0,
    nodestruct:[],
    modalIsOpen1:false,
    mul:[]
  };

  componentDidMount() {
    this.getForumStructure();
  }
  openModal = () => {
    this.setState({modalIsOpen: true});
  };

  closeModal = () => {
    this.setState({modalIsOpen: false, srcimage: ""});
  };
  openModal1 = () => {
    this.setState({modalIsOpen1: true});
  };

  closeModal1 = () => {
    this.setState({modalIsOpen1: false, srcimage: ""});
  };
  getForumStructure() {


    fetch(`${process.env.API_URL}/forum/structure`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 404 || response.status ===  500) {
        console.log('Сервер не отвечает');
        return;
      }

      if (response.status === 200) {

        response.json().then(data => {
          let structure = [];
          for (let i = 0; i < data.data.structure.length; i++) {
            let structTheme = data.data.structure[i];
            structure.push({
              title: structTheme.root,
              name: structTheme.root,
              expanded: true,
              id: structTheme.id,
              index: [i],
              children: structTheme.subroot.map((subrootLevelFirst, firstLevelIndex) => {
                let children;
                if (subrootLevelFirst.subroot) {
                  children = subrootLevelFirst.subroot.map((subrootLevelSecond, secondLevelIndex) => {
                    return {
                      title: subrootLevelSecond.root,
                      name: subrootLevelSecond.root,
                      id: subrootLevelSecond.id,
                      index: [i, firstLevelIndex, secondLevelIndex],
                      expanded: true
                    }
                  });
                }

                let sub = {
                  title: subrootLevelFirst.root,
                  name: subrootLevelFirst.root,
                  id: subrootLevelFirst.id,
                  index: [i, firstLevelIndex],
                  expanded: true
                };

                if (children)
                  sub.children = children;

                return sub;
              })
            })
          }

          this.setState({structure: structure, isAPILoaded: true}, ()=>{this.closeModal1();});
        });
      }
    }).catch(err => {
      this.closeModal1();
      console.log(err);
    });

  };
  addtstruct =() => {
    this.openModal1();
    if (document.getElementById('AddName').value === "")
      return "";
    else {
      if (this.state.typeOpenModal === 0) {
        let name=document.getElementById('AddName').value;
        fetch(`${process.env.API_URL}/forum/global-theme/${this.props.user.info._id}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          body: `title=${name}`
        }).then(response => {
          if (response.status === 404 || response.status === 500) {
            console.log('Сервер не отвечает');
            this.closeModal1();
            return "";
          }

          if (response.status === 200) {
            this.setState(state => ({
              structure: state.structure.concat({
                title: name,
                name: name
              }),
            }));
            this.getForumStructure();
          }
        }).catch(err => {
          this.closeModal1();
          console.log(err);
        });
      }

      if(this.state.typeOpenModal === 1)
      {
        this.openModal1();
        let val=document.getElementById('AddName').value;
        fetch(`${process.env.API_URL}/forum/sub-theme/${this.props.user.info._id}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          body: `name=${val}&parentInfo=${ JSON.stringify(this.state.nodestruct)}`
        }).then(response => {
          if (response.status === 404 || response.status === 500) {
            console.log('Сервер не отвечает');
            this.closeModal1();
            return "";
          }

          if (response.status === 200) {
            this.state.mul.newNode.title = val;
            this.state.mul.newNode.name = val;
            let dat = addNodeUnderParent(this.state.mul).treeData;
            this.setState({structure: (dat)});
            this.getForumStructure();
          }
        }).catch(err => {
          console.log(err);});
      }
      this.closeModal();
    }
  };

  addSubTheme = () => {
    this.setState({typeOpenModal: 0}, () => {
      this.openModal();
    });
  };

  addSubThemes = (dat) => {
    this.setState({typeOpenModal: 1, nodestruct: dat}, () => {
      this.openModal();
    });
  };

  RemoveSubThemes = (dat) => {

    this.setState({typeOpenModal: 2,nodestruct: dat}, () => {
      this.openModal();
    });
  };

  RemoveSubThem = () =>{
    this.openModal1();
    fetch(`${process.env.API_URL}/forum/theme/${this.props.user.info._id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: `parentInfo=${ JSON.stringify(this.state.nodestruct)}`
    }).then(response => {
      if (response.status === 404 || response.status === 500) {
        console.log('Сервер не отвечает');
        return "";
      }

      if (response.status === 200) {

        this.setState({structure: this.state.mul});
        this.getForumStructure();
      }
    }).catch(err => {
      console.log(err);
    });

    this.closeModal();
  };

  render() {
    const customStylesModal = {
      content : {
        padding: '50px',
        display:'flex',
        height:'100%',
        justifyContent: 'center',
        algorithm: 'center',
        flexDirection: 'column'

      },

      overlay: {
        background: 'rgba(0, 0, 0, 0.6)'
      }
    };

    const getNodeKey = ({ treeIndex }) => treeIndex;
    return (
      <div style={{position:'relative'}}>
      <div style={{height: 600}}>

        {this.state.isAPILoaded && this.state.structure.length > 0 ?
          <SortableTree
            canDrag={false}
            treeData={this.state.structure}
            onChange={structure => this.setState({ structure })}
            generateNodeProps={({ node, path }) => ({
              title: (<div style={{ fontSize: '1.1rem' }}>{node.name}</div>),
              buttons: [
                <Icon
                  name='plus'
                  className={`forum-structure__actions forum-structure__actions-add ${path.length > 2 ? 'forum-structure__actions-add--hidden' : ''}`}
                  onClick={() => {
                    console.log(node);
                    this.setState ({
                      mul: {
                        treeData: this.state.structure,
                        parentKey:path[path.length - 1],
                        expandParent:true,
                        index: node.index.concat([node.children.length]),
                        getNodeKey,
                        newNode: {
                          title: `dat`,
                          name: `dat`
                        }
                      }
                    }, () => { this.addSubThemes(node);
                       })
                  }}
                />,

                <Icon
                  name='minus'
                  className='forum-structure__actions forum-structure__actions-remove'
                  onClick={() => {
                    this.RemoveSubThemes(node);
                    this.setState(state => ({
                      mul: removeNodeAtPath({
                        treeData: state.structure,
                        path,
                        getNodeKey,
                      }),
                    }))
                  }}
                />
              ]
            })}
          />
          : null}
      </div>
        <Button onClick={this.addSubTheme} text='Добавить категорию' className='button--primary-ghost forum__add-subtheme'/>
        <Modal shouldCloseOnOverlayClick={true} style={customStylesModal} isOpen={this.state.modalIsOpen} onRequestClose={this.closeModal}  className={"ForumConsultation__modal"}>
          <Card className={"ForumConsultation__modal__card"}>
            <Icon name={"times-circle"} onClick={this.closeModal} className={"ForumConsultation__modal__card__close"}/>
            {this.state.typeOpenModal === 2 ?
              <div className={"ForumConsultation__modal__card__Delete"}>
                <h1 className={"ForumConsultation__modal__card__Delete__header"}>Хотите ли вы удалить тему</h1>
                <div className={"ForumConsultation__modal__card__Delete__BtBlock"}>
                  <Button className=" button--gray-ghost dialog__send ForumConsultation__modal__card__Delete__BtBlock__button" text="Не удалять"  onClick={ this.closeModal}/>
                  <Button className=" button--orange-ghost dialog__send ForumConsultation__modal__card__Delete__BtBlock__button" text="Удалить" onClick={this.RemoveSubThem}/>
                </div>
              </div>
              : <div className={"ForumConsultation__modal__card"}>
              <h1 className={"ForumConsultation__modal__card__header"}>{this.state.typeOpenModal===0?"Создание категории":"Создание темы/подтемы"}</h1>
              <div className={"ForumConsultation__modal__card__content"}>
                <p>Наименование</p>
                <InputSimple id="AddName" isMultiline={true} rows="1" cols="50"/>
                <Button  className=" button--primary-ghost dialog__send" text="Создать" onClick={()=>this.addtstruct()}/>
              </div>
            </div>}

          </Card>
        </Modal>
        <Modal shouldCloseOnOverlayClick={true} style={customStylesModal} isOpen={this.state.modalIsOpen1} onRequestClose={this.closeModal1}  className={" ForumDownloader "}>
          <Loader type="ball-pulse"   active />
          <h2>Загрузка</h2>
        </Modal>
      </div>

    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu,
    user: state.user
  }),

  dispatch => ({})
)(ForumConsultation);

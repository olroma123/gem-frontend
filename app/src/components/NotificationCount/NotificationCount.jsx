import React from 'react';
import './NotificationCount.scss';

export default class NotificationCount extends React.Component {
  render() {
    return (
      <div className={'notification__count ' +
        (this.props.isPrimary ? 'notification__count--white ' : 'notification__count--gray ') +
        (this.props.count === 0 ? 'notification__count--hidden ' : '') +
        (this.props.className || '')}>
        {this.props.count}
      </div>
    );
  }
}

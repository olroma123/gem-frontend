import React from 'react';
import GemoLogo from '../GemoLogo';
import FooterNav from '../FooterNav';
import FooterNavItem from '../FooterNavItem';
import { connect } from 'react-redux';

// Bootstrap
import Col from '../Col';
import Container from '../Container';
import Row from '../Row';

import "./Footer.scss";

class Footer extends React.Component {
  render() {
    return (
    	<Container fluid={true}>
    		<Row className={"footer " + (this.props.isAuthPage ? 'footer--auth-page' : '')}>
    			<Col lg="7" xs="12">
    				<FooterNav>
              <FooterNavItem link="/help" text="Помощь"/>
            </FooterNav>
    			</Col>

    			<Col lg="5" xs="12" className="footer__logo-column">
            <GemoLogo redirect='/' isPrimary={false}/>
    			</Col>
    		</Row>
    	</Container>
    )
	}
}
export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({})
)(Footer);

import React from 'react';
import {Link} from 'react-router-dom';
import './ProfileTipItem.scss';

class ProfileTipItem extends React.Component {
  render() {
    return (
      <li className={(this.props.className || '') + ' profile-tips__item'}>

        <div className="profile-tips__circle">
          <img src={this.props.iconPath} alt="icon" />
        </div>

        <div>
          <span className="profile-tips__title">{this.props.title}</span>
          <span className="profile-tips__description">{this.props.text}</span>
          <Link to={this.props.link || '#'} className="profile-tips__act">{this.props.act}</Link>
        </div>

      </li>
    );
  }
}

export default ProfileTipItem;
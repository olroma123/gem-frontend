import React from 'react';
import {NavLink} from 'react-router-dom';
import Button from '../Button';
import Form from '../Form';
import Input from '../Input';
import APIMessage from '../APIMessage';

// Bootstrap
import Col from '../Col';
import Row from '../Row';

import './RegistrationForm.scss';

class RegistrationForm extends React.Component {
  constructor() {
    super(...arguments);

    this.state = {
      isLoginSuccess: false,
      errorText: '',
      roleItems: ['1', '2'],
      emailLabel: 'Email',
      passwordLabel: 'Пароль',
      passwordRepeatLabel: 'Потверждение пароля',
      firstnameLabel: 'Имя',
      surnameLabel: 'Фамилия'
    }
  }

  render() {
    let regex = {
      name:/^[а-яА-Я]+$/,
      surname: /[а-яА-ЯёЁ]/,
      email: /^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/,
      password: /[a-zA-Z0-9]{8}/
    };

    let apiReg = () => {
      let user = {
        email: document.getElementById('email').value,
        password: document.getElementById('password').value,
        firstname: document.getElementById('firstname').value,
        surname: document.getElementById('surname').value,
        role: document.getElementById('role').value
      };
      let repeatPassword = document.getElementById('passw__double').value;
      let isValidationSuccess = true;
      let setInputError = (id) => {
        document.getElementById(id).classList.add('error');
        isValidationSuccess = false;
      }

      // Validate all input's
      if (!regex.email.test(user.email)) {
        this.setState({emailLabel: 'Неправильный email!'});
        setInputError('email_block');
      } if (!regex.name.test(user.firstname)) {
        this.setState({firstnameLabel: 'Неправельное имя!'});
        setInputError('firstname_block');
      } if (!regex.name.test(user.surname)) {
        this.setState({surnameLabel: 'Неправильная фамилия!'});
        setInputError('surname_block');
      } if (!regex.password.test(user.password)) {
        this.setState({passwordLabel: 'Неправильный пароль!'});
        setInputError('password_block');
      } if (!user.password === repeatPassword) {
        this.setState({passwordLabel: 'Пароли не совпадают!'});
        document.getElementById('password__double_block').classList.add('error');
        isValidationSuccess = false;
        return;
      }

      if (!isValidationSuccess)
        return;

      fetch(`${process.env.API_URL}/user`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: `email=${user.email}&password=${user.password}&firstname=${user.firstname}&surname=${user.surname}&role=${user.role}`
      }).then((response) => {
        if (response.status === 404 || response.status ===  500) {
          this.setState({errorText: 'Сервер не отвечает'});
          return;
        }

        response.json().then((data) => {
          this.setState({errorText: data.message.message, isLoginSuccess: response.status === 200});
        });
      }).catch((response) => {
        throw new Error('An error was occured. Object for debug: ' + response);
      });
    };

    // Get role name from API
    let getRole = () => {
      return new Promise ((resolve, reject) => {
        fetch(`${process.env.API_URL}/roles`, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
        }).then((response) => {
          if (response.status === 404 || response.status === 500) {
            reject();
            return;
          }

          resolve(new Promise ((resolve, reject) => {
            response.json().then((data) => {
              resolve(data);
            });
          }));
        }).catch((response) => {
          reject(new Error('An error was occured. Object for debug: ' + response));
        });
      })
    };

    // Fill role input with data
    if (this.state.roleItems[0] === '1') {
      getRole().then((data) => {
        let arrayWithRole = data.data.map(role => { return role.text });
        window.sessionStorage.setItem('roles', arrayWithRole.join('&'));
        this.setState({roleItems: arrayWithRole});
      }).catch((error) => {
        this.setState({roleItems: window.sessionStorage.getItem('roles') ?
            window.sessionStorage.getItem('roles').split('&') :
            ['Пользователь', 'Специалист-консультант']
        });
      });
    }

    return (
      <Form submitCallback={apiReg} className="login__form" >
        <APIMessage status={this.state.isLoginSuccess ? 'success' : 'error'} text={this.state.errorText}/>
        <Row style={{marginTop: '20px'}}>
          <Col xs="12" md="6" className="registration__left">
            <Input idBlock="firstname_block" validateRegex={regex.name} required={true} id="firstname" className="registration__left__input" type="text" labelText={this.state.firstnameLabel}/>
            <Input idBlock="email_block" validateRegex={regex.email} required={true} id="email" className="registration__left__input" type="text" labelText={this.state.emailLabel}/>
            <Input idBlock="password_block" validateRegex={regex.password} required={true} id="password" className="registration__left__input" type="password" labelText={this.state.passwordLabel}/>
          </Col>

          <Col xs="12" md="6" className="registration__right">
            <Input idBlock="surname_block" validateRegex={regex.name} required={true} id="surname" className="registration__right__input" type="text" labelText={this.state.surnameLabel}/>
            <Input items={this.state.roleItems.join('&')} required={true} id="role" className="registration__right__input" type="select" labelText="Роль"/>
            <Input validateRegex={regex.password} required={true} id="passw__double" idBlock="password__double_block" className="registration__right__input" type="password" labelText={this.state.passwordRepeatLabel}/>
          </Col>
        </Row>

        <Button type="submit" text="ЗАРЕГИСТРИРОВАТЬСЯ" className="button--primary registration__submit"/>
        <h2 className="registration__certificate">Нажимая кнопку «Зарегистрироваться», вы подтверждаете своё<br/>согласие с
          <NavLink to="#"> политикой конфиденциальности </NavLink> сайта.
        </h2>

        <h3 className="registration__have_account__text">Уже есть аккаунт?</h3>
        <NavLink className="registration__have_account" to={`${process.env.URL_FOLDER}login`}>Войти</NavLink>
      </Form>
    );
  }
}

export default RegistrationForm;

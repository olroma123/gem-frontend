import React from 'react';
import {connect} from 'react-redux';
import {Icon} from 'react-fa';
import InputSimple from '../../InputSimple';
import AvatarCircle from "../../AvatarCircle/AvatarCircle";
import Button from '../../Button';
import './Item.scss';
import Modal from 'react-modal';
import Card from '../../Card';
import DialogMessage from  '../../DialogMessage';
import moment from "moment/moment";

class Item extends React.Component {
  state = {
    Message:[],
    modalIsOpen: false
  };

  closeModal = () => {
    this.setState({modalIsOpen: false});
  };

  openModal = () => {
    this.setState({modalIsOpen: true});
  };

  GetMessage = (id) => {
    fetch(`${process.env.API_URL}/doctor/message/${this.props.user.info._id}?userID=${id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response =>  {
      response.json().then(data => {
        this.setState({Message: data.data});
        this.openModal();
      });
    });
  };

  Datetime = (dat) => {
    let diff = moment(new Date()).diff(moment(new Date(dat)), 'days');
    let date = moment(new Date(dat)).format('DD.MM.YYYY');
    return diff < 2 ? moment(new Date().fromNow(new Date(dat))).fromNow() : date;
  };

  AddMessage = () => {
    let dat = document.getElementById('SendMessageUser').value;
    fetch(`${process.env.API_URL}/doctor/message/${this.props.user.info._id}?userID=${this.props.link} `, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: `text=${dat}`
    }).then(response => {
      document.getElementById('SendMessageUser').value="";
      this.GetMessage(this.props.link);
    });
  };

  render() {
    const customStylesModal = {
      content : {
        padding: '50px',
        height:'100%',
        algorithm: 'center',
        flexDirection: 'column',
        width:'100%'
      },
      overlay: {
        background: 'rgba(0, 0, 0, 0.6)'
      }
    };

    return (
      <div className={"TretmantSpecialistWork__Item"}>
        <a href={`/id/${this.props.link}`}>
          <div className={"TretmantSpecialistWork__Item__row"}>
            <AvatarCircle img={this.props.img} className={"TretmantSpecialistWork__Item__row__avatar"}/>
            <h2 className={"TretmantSpecialistWork__Item__row__header"}>{this.props.name}</h2>
          </div>
        </a>

        <div className={"TretmantSpecialistWork__Item__row__2"}>
          <Button onClick={() => this.GetMessage(this.props.link)} className="button--primary-ghost" text="Открыть чат" />
        </div>

        <Modal shouldCloseOnOverlayClick={true} style={customStylesModal} isOpen={this.state.modalIsOpen} onRequestClose={this.closeModal} className={" ForumDownloader "}>
          <Card cardShadow='no-shadow' className={"TretmantSpecialistWork__Item__HeaderRow__block"}>
            <div className={"TretmantSpecialistWork__Item__HeaderRow"}>
              <div className={"TretmantSpecialistWork__Item__HeaderRow__cancel"} onClick={this.closeModal}>
                <Icon name={"angle-left"}/> Назад
              </div>

              <AvatarCircle img={this.props.img} className={"TretmantSpecialistWork__Item__HeaderRow__avatar"}/>
              <h2 className={"TretmantSpecialistWork__Item__HeaderRow__header"}>
                <a href={`/id/${this.props.link}`}>{this.props.name}</a>
              </h2>
            </div>

            <div className={"TretmantSpecialistWork__Item__HeaderRow__body"}>
              {
                this.state.Message && this.state.Message.length > 0 ?
                  this.state.Message.map(item=>(<DialogMessage key={item._id} image={item.userFrom.avatar} name={item.userFrom.fullName} dateAgo={this.Datetime(item.date)} text={item.text} />)):<div/>
              }
            </div>

            <div className={"TretmantSpecialistWork__Item__HeaderRow__NewMessage"}>
              <InputSimple id="SendMessageUser" isMultiline={true} rows="1" cols="90"/>
              <Button className=" button--primary-ghost " text="Отправить сообщение" onClick={this.AddMessage} />
            </div>
          </Card>
        </Modal>
      </div>
    );
  }
}
export default connect(
  state => ({
    user: state.user
  }),
)(Item);
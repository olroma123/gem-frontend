import React from 'react';
import {connect} from "react-redux";
import Item from './Item';
import User from './User';
import TabControl from '../TabControl';
import CardTitle from '../Card/CardEmptyTitle';
import {Tab, TabPanel, TabList} from 'react-web-tabs';
import './TreatmentSpecialistWork.scss';

class TreatmentSpecialistWork extends React.Component {
  state = {
    Pending: [],
    Message: [],
    User: [],
    work: false
  };

  componentWillMount()  {
    fetch(`${process.env.API_URL}/doctor/status/${this.props.user.info._id}?status=${'pending'} `, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response =>  {
      response.json().then(data => {
        this.setState({Pending: data.data});
        this.GetUser();
      })
    });
  }

  GetUser = () => {
    fetch(`${process.env.API_URL}/doctor/status/${this.props.user.info._id}?status=${`accepted`}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response =>  {
      response.json().then(data => {
        this.setState({User: data.data});
      });
    });
  };

  render() {
    let usersChat, usersRequested;
    if (this.state.User && this.state.User.length > 0) {
      usersChat = this.state.User.map(item => (<User key={item.userID} name={item.fullName} link={item.userID} img={item.avatar} />));
    }
    else{
      usersChat =(<CardTitle>Нет сообщений с пациентами</CardTitle>)
    }

    if (this.state.Pending && this.state.Pending.length > 0) {
      usersRequested = this.state.Pending.map(item => (<Item key={item.userID} name={item.fullName} link={item.userID} img={item.avatar} />));
    } else usersRequested = (<CardTitle>Нет запросов на добавление</CardTitle>);

    return (
      <TabControl>
        <TabList>
          <Tab tabFor="one">Общение</Tab>
          <Tab tabFor="three">Запросы</Tab>
        </TabList>

        <TabPanel tabId="one">
          {usersChat}
        </TabPanel>

        <TabPanel tabId="three">
          {usersRequested}
        </TabPanel>
      </TabControl>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  })
)(TreatmentSpecialistWork);
import React from 'react';
import InputSimple from '../InputSimple';
import Button from '../Button';
import './DialogFooter.scss';

export default class DialogFooter extends React.Component {
  render() {
    return (
      <div className='dialog__footer'>
        <InputSimple isMultiline={true} rows="1" cols="50"/>
        <Button className="button--primary" text="Отправить"/>
      </div>
    );
  }
}

import React from 'react';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import "./BigCalendar.scss";

export default class Bigcalendar extends React.Component {
  render() {
    const Colordate = (event) => {
      switch(event.type) {
        case "Онлайн-консультация": {
          return {
            backgroundColor: '#2ec4b6',
            color: '#2ec4b6'
          };
        }

        case "Профилактика": {
          return{
              backgroundColor: '#e37550',
              color: '#e37550'
            };
        }

        case "Осмотр": {
          return{
            backgroundColor: '#28bbdb',
            color: '#28bbdb'
          };
        }

        case "Лечение": {
          return{
            backgroundColor: '#ce45c0',
            color: '#ce45c0'
          };
        }

        default: {
          return {
            backgroundColor: '#2ec4b6',
            color: '#2ec4b6'
          };
        }
      }
    };

    BigCalendar.momentLocalizer(moment);
    return (
      <div className="Bigcalendar">
          <BigCalendar
            defaultDate={new Date()}
            events={this.props.event.length === 0 ? [] : this.props.event}
            selectable={true}
            startAccessor='startDate'
            endAccessor='endDate'
            className="Bigcalendar-cal"
            drilldownView="agenda"
            eventPropGetter={(Colordate)}
          />
      </div>
    );
  }
}

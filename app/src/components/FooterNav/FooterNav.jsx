import React from 'react';
import './FooterNav.scss';

 export default class FooterNav extends React.Component {
  render() {
    return (
      <ul className="footer__nav">
        {this.props.children}
      </ul>
    );
  }
}

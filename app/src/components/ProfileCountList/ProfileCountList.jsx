import React from 'react';
import './ProfileCountList.scss';

class ProfileCountList extends React.Component {
  render() {
    return (
      <ul className='profile-info__list'>
        {this.props.children}
      </ul>
    );
  }
}

export default ProfileCountList;
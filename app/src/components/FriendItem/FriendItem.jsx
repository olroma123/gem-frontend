import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import AvatarCircle from '../AvatarCircle';
import './FriendItem.scss';

export default class FriendItem extends Component {
  render() {
    let link;
    if (this.props.isFriendRequest) {
      const acceptFriend = () => {
        fetch(`${process.env.API_URL}/friend/${this.props.token}/accept/${this.props.id}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        }).then(response => {
          if (response.status === 404 || response.status ===  500) {
            console.warn('Нет подключения к API');
          } else if (response.status === 200) {
            if (this.props.acceptHandler) this.props.acceptHandler();
            else window.location.reload();
          }
        }).catch(err => {
          console.warn(err);
        });
      };

      link = (<div className='friends__link' onClick={acceptFriend}>Добавить в друзья</div>);
    } else {
      link = (<div className='friends__link' onClick={() => window.location.href = `/dialog/${this.props.id}`}>{'Написать сообщение'}</div>);
    }

    return (
      <li className='friends__item'>
        <AvatarCircle img={this.props.avatar}/>
        <Link className='friends__full-name' to={`/id/${this.props.id}`}>{this.props.fullName}</Link>
        {link}
      </li>
    );
  }
}

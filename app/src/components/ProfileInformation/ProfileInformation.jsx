import React from 'react';
import Card from '../Card';
import { connect } from 'react-redux';
import {Icon} from 'react-fa';
import ProfileInformationPersonal from '../ProfileInformationPersonal/ProfileInformationPersonal';
import ProfileInformationPublic from '../ProfileInformationPublic/ProfileInformationPublic';
import ProfileInformationAbout from '../ProfileInformationAbout/ProfileInformationAbout';
import ProfileCountList from '../ProfileCountList/ProfileCountList';
import ProfileCountItem from '../ProfileCountItem/ProfileCountItem';

import Item from '../Item/Item';
import './ProfileInformation.scss';

class ProfileInformation extends React.Component {
  state = {
    countFriends: 0,
    countTopics: 0,
    isAPILoaded: false,
    userInfo: {},
    PostInfo: {}
  };

  _getCountTopics = () => {
    fetch(`${process.env.API_URL}/forum/user-count/${this.props.userID || this.props.user.info.id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({PostInfo: ''});
        return;
      }

      response.json().then(data => {
        this.setState({countTopics: response.status === 200 ? data.data.countThemes : 0});
      }).catch(err => {
        throw new Error(err);
      })
    }).catch(err => {});
  };

  componentDidMount() {
    fetch(`${process.env.API_URL}/post/${this.props.user.info.id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({PostInfo: ''});
        return;
      }

      response.json().then(data => {
        this.setState({PostInfo: response.status === 200 ? data.data.posts : ''});
      }).catch(err => {
        throw new Error(err);
      })
    }).catch(err => {
      window.location.replace(`${process.env.URL_FOLDER}Error500`);
    });

    this._getCountTopics();

    const id = this.props.userID ? `userID=${this.props.userID}` : `token=${this.props.user.info._id}`;
    fetch(`${process.env.API_URL}/user/public?${id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        console.warn('Сервер не отвечает');
        return;
      }

      response.json().then(data => {
        this.setState({isAPILoaded: true, userInfo: response.status === 200 ? data.data : data.message.message});
      });
    }).catch(err => {
      throw new Error(err);
    });
  }

  handleProfileChange = () => {
    this.componentDidMount();
  };

  render() {
     let fullName = '';
     if (this.state.isAPILoaded)
       fullName = `${this.state.userInfo.fullName.surname || ''} ${this.state.userInfo.fullName.name || ''} ${this.state.userInfo.fullName.lastname || ''}`;
     else
       fullName = '';

     return (
        <Card cardShadow='no-shadow' className="profile-info">
        <ProfileInformationPersonal handleChange={this.handleProfileChange} avatar={this.props.avatar} isEditable={!this.props.userID} userID={this.props.userID} status={this.state.userInfo.status} name={fullName} />

        <ProfileInformationPublic className="profile-info__public">
          <Item className={this.state.userInfo.birthday ? '' : 'no-info'} showIcon={true} iconWidth='15' iconName='calendar' text={this.state.userInfo.birthday !== '' ? new Date().normalizeDateString(new Date(this.state.userInfo.birthday)) : '(нет информации)'}/>
          <Item className={this.state.userInfo.city ? '' : 'no-info'} showIcon={true} iconWidth='15' iconName='map-marker' text={this.state.userInfo.city || '(нет информации)'}/>
          <Item className={this.state.userInfo.hemoStatus ? '' : 'no-info'} showIcon={true} iconWidth='15' iconName='plus' text={this.state.userInfo.hemoStatus || '(нет информации)'}/>
          <Item className={this.state.userInfo.role ? '' : 'no-info'} showIcon={true} iconWidth='15' iconName='asterisk' text={this.state.userInfo.role || '(нет информации)'}/>
        </ProfileInformationPublic>

        <ProfileInformationAbout className={this.state.userInfo.about ? '' : 'no-info'}>{this.state.userInfo.about || '(нет информации)'}</ProfileInformationAbout>

        <h4 className='profile__social-title'>Социальные сети</h4>
        <ul className="profile__social-links">
          <li className="item">
            <Icon name='vk'/>
            <a className={this.state.userInfo.social && this.state.userInfo.social.vk !== '' ? '' : 'no-info'} href={this.state.userInfo.social ? this.state.userInfo.social.vk : '#'}>{this.state.userInfo.social && this.state.userInfo.social.vk !== '' ? this.state.userInfo.social.vk : '(нет информации)'}</a>
          </li>

          <li className="item">
            <Icon name='facebook'/>
            <a className={this.state.userInfo.social && this.state.userInfo.social.facebook !== '' ? '' : 'no-info'} href={this.state.userInfo.social ? this.state.userInfo.social.facebook : '#'}>{this.state.userInfo.social && this.state.userInfo.social.facebook !== '' ? this.state.userInfo.social.facebook : '(нет информации)'}</a>
          </li>

          <li className="item">
            <Icon name='google-plus'/>
            <a className={this.state.userInfo.social && this.state.userInfo.social.googlePlus !== '' ? '' : 'no-info'} href={this.state.userInfo.social ? this.state.userInfo.social.googlePlus : '#'}>{this.state.userInfo.social && this.state.userInfo.social.googlePlus !== '' ? this.state.userInfo.social.googlePlus : '(нет информации)'}</a>
          </li>
        </ul>
        <ProfileCountList>
          <ProfileCountItem count={this.state.userInfo.countFriends} name='друзей' link="/friends?tab=all"/>
          <ProfileCountItem count={this.state.countTopics} name='тем на форуме' link={"/forum"}/>
          <ProfileCountItem count={this.state.PostInfo.length} name='записей в ленте'/>
        </ProfileCountList>
      </Card>
    );
  }
}
export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({})
)(ProfileInformation);

import React from 'react';
import './LoginSocialGroup.scss';

class LoginSocialGroup extends React.Component {
  render() {
    return (
      <div className="login__social-login">
        {this.props.children}
      </div>
    );
  }
}

export default LoginSocialGroup;
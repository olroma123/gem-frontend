import React, {Component} from 'react';
import {Icon} from 'react-fa';
import './PDF.scss';

export default class PDF extends Component {
  render() {
    return (
      <div onClick={this.props.onClick} className={'pdf-decorator ' + (this.props.className || '')}>
        <div className="pdf-decorator__text">
          PDF

          <Icon name='download'/>
        </div>
      </div>
    );
  }
}


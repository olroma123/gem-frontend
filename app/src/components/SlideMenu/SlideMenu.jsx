import React from 'react';
import Card from '../Card';
import './slide.scss';
import Item from "../Item";
import SlideMenuNav from "../SlideMenuNav";
import Col from "../Col";
import {connect} from "react-redux";
import {Icon} from 'react-fa';
const UserAction = require('../../actions/UserAction.js');

class SlideMenu extends React.Component {
  state = {
    collapsed: true,
    type: -1
  };

  componentDidMount() {

    if(this.state.type === -1)
    fetch(`${process.env.API_URL}/security/${this.props.user.info._id}/admin`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 200)
        this.setState({type: 0});
      else
        this.setState({type: -1});
    }).catch(err => {
      throw new Error(err);
    });

    }

  toggleMenu = () => {
    this.setState({collapsed: !this.state.collapsed});
  };
  render() {
    const colapse = this.state.collapsed ? 'slide-menu' : `slide-menu--active`;
    const item = this.state.collapsed ? 'slide-menu-icon' : `slide-menu-icon--active`;
    return (
      <Col className={(this.props.className || '') + " SlideMen"} style={{display: this.props.menuState.open ? 'flex' : 'none', flexDirection: 'column', alignItems: 'center'}} sm="1" lg={"3"}>
        <Card cardShadow='no-shadow' className= {(this.props.classNames || '') + ` ${colapse}`}>
          <div className={ item } onClick={this.toggleMenu} > {this.state.collapsed ? <Icon  name={"caret-right"} /> : <div className={"SlideMenu__cancel"}> <Icon  name={"arrow-left"} /> <span className={"arrow-left__text"}> Свернуть  </span> </div> } </div>
          <SlideMenuNav>
            <Item isActive={window.location.href.includes('/home')} isLink={true} link={`${process.env.URL_FOLDER}home`} showIcon={true} margin="15" iconName="home" text="Моя страница"/>
            <Item isActive={window.location.href.includes('/treatment') && !window.location.href.includes('treatment-')} isLink={true} link={`${process.env.URL_FOLDER}treatment`} showIcon={true} margin="15" iconName="calendar" text="Журнал"/>
            <Item isActive={window.location.href.includes('/treatment-plan')} isLink={true} showIcon={true} margin="15" iconName="plus" link={`${process.env.URL_FOLDER}treatment-plan`} text="План лечения"/>
            <Item isActive={window.location.href.includes('/dialog')} notificationCount={this.props.notification.messages} isLink={true} link={`${process.env.URL_FOLDER}dialogs`} showIcon={true} margin="15" iconName="envelope" text="Сообщения"/>
            <Item isActive={window.location.href.includes('/friends')} notificationCount={this.props.notification.friends} isLink={true} link={`${process.env.URL_FOLDER}friends`} showIcon={true} margin="15" iconName="user" text="Друзья"/>
            <Item isActive={window.location.href.includes('/forum')} isLink={true} link={`${process.env.URL_FOLDER}forum`} showIcon={true} margin="15" iconName="group" text="Форум"/>
            <Item isActive={window.location.href.includes('/profileEdit')} isLink={true} link={`${process.env.URL_FOLDER}profileEdit`} showIcon={true} margin="15" iconName="cog" text="Настройки"/>
            {
              this.state.type === 0 ? <Item isActive={window.location.href.includes('/admin')} isLink={true} link={`${process.env.URL_FOLDER}admin`} showIcon={true} margin="15" iconName="lock" text="Администрирование"/>
                :<span/>
            }
            <Item isActive={false} onClick={this.props.onLogout} isLink={true} showIcon={true} margin="15" iconName="power-off" text="Выйти"/>
          </SlideMenuNav>
        </Card>
      </Col>
    );
  }
}

export default connect(
  state => ({
    user: state.user,
    notification: state.notification,
    menuState: state.slideMenu
  }),

  dispatch => ({
    onLogout: () => {
      dispatch(UserAction.default.logOut());
    }
  })
)(SlideMenu);
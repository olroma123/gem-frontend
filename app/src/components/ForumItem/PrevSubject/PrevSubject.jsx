import React from 'react';
import Subject from "../Subject";
import Commit from  "../Commit";
import "./PrevSubject.scss";
import CardTitle from '../../Card/CardEmptyTitle';

class PrevSubject extends React.Component {
  constructor() {
    super(...arguments);
  }
  WorkDate = (date)=>
{
  let Month=['января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'];
  let Normaldate =  new Date(date).getDate()+" "+Month[new Date(date).getMonth()+1]+" "+new Date(date).getFullYear();
  return Normaldate;
};
  render() {
    console.log(this.props.element);
    return (
      <div className={"ForumItem"}>
        {
          this.props.id.length<4?
            <div>
              <h3 className={"ForumItem__header"}>Подразделы</h3>
                {
                  this.props.element.subtheme && this.props.element.subtheme.length > 0  ?
                   this.props.element.subtheme.map(item => (
                   <Subject key={item.id} id={item.id} header={item.root} str={item.subroot || []} LAuthor={"Ilya"}count={item.count}  />)) :
                    <div className={"Forum--none"}><CardTitle>Нет разделов для выбранной темы</CardTitle></div>
                }
            </div>
            :""
        }
        <h3 className={"ForumItem__header"}>Разделы</h3>
        {
          this.props.element.topics && this.props.element.topics.length>0 ?
            this.props.element.topics.map(item => (
              <Commit key={item._id} href={item.owner.id} image={item.owner.avatar} topicid={item._id} header={item.title} Author={item.owner.fullName} date={this.WorkDate(item.dateCreated) } olddate={(item.comments.last?this.WorkDate(item.comments.last.date):"")} comments={item.comments.last||[]} answer={item.comments.count} /> ))  :
            <div className={"Forum--none"}><CardTitle>Нет топиков для выбранной темы</CardTitle></div>
        }

      </div>
    );
  }
}

export default PrevSubject;
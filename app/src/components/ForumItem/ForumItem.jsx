import React from 'react';
import Subject from "./Subject";
import CardTitle from '../Card/CardEmptyTitle';
import "./ForumItem.scss";

class ForumItem extends React.Component {
  state = {
    strings:[]
  };

  componentWillMount() {
   if (this.props.element.length > 0) {
      let subTheme = this.props.element.map(theme => theme.root);
      this.setState({strings: subTheme})
    } else this.setState({strings:[]});
  }

  GetCount = (id) => {
    let count = 0;
    for (let item of this.props.count) {
      if (item.id === id) count = item.count;
    }

    return count;
  };

  render() {
    return (
        <div className={"ForumItem"}>
          <h3 className={"ForumItem__header"}>{this.props.text}</h3>
          {
            this.props.element.length>0?
              this.props.element.map(item => (
                <Subject key={item.id} id={item.id} header={item.root} count={this.GetCount(item.id)}   str={item.subroot || []} LAuthor={"Ilya"} date={"Сегодня в 6 часов"} href={"#"} />
              )):<div className={"Forum--none"}><CardTitle>Нет разделов для этой темы</CardTitle></div>
          }
        </div>
    );
  }
}

export default ForumItem;
import React from 'react';
import './ProfileCountItem.scss';

class ProfileCountItem extends React.Component {
  render() {
    return (
      <li className='profile-info__count'>
        <a href={this.props.link}>
        <span>{this.props.count}</span>
        <span>{this.props.name}</span>
        </a>
      </li>
    );
  }
}

export default ProfileCountItem;
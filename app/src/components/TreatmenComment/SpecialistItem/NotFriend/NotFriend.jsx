import React, { Component } from 'react';
import {Icon} from 'react-fa';
import './NotFriend.scss';

export default class NotFriend extends Component {
  render() {
    return (
      <div className={"treatment-specialist--not-friend"}>
        <Icon name="exclamation-circle"/> 
        {this.props.pending && this.props.pending.lenght > 0 ? "Запрос на добавление в друзья был отправлен" : "Пользователь не в друзьях"}
      </div>
    );
  }
}

import React, { Component } from 'react';
import AvatarCircle from '../../../AvatarCircle';
import {Icon} from 'react-fa';
import Button from '../../../Button';
import NotFriend from '../NotFriend/NotFriend';
import './SpecialistChoosen.scss';

export default class SpecialistChoosen extends Component {
  render() {
    return (
      <div className={"treatment-doctor"}>
        <AvatarCircle img={this.props.doctor.avatar} className="treatment-doctor__avatar"/>

        <div className={"treatment-doctor__user-info"}>
          <div className={"treatment-doctor__user-fullName"}>{this.props.doctor.fullName}</div>
          {!this.props.doctor.isFriends ? <NotFriend pending={this.props.pending}/> : null}
        </div>

        <Button onClick={this.props.handleAddDoctor} text='Выбрать этого врача' className='button--primary-small-round treatment-doctor__choose'/>
      </div>
    );
  }
}

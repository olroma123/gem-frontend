import React from 'react';


import './position.scss';

export default class PositionThird extends React.Component {
  render() {
    return (
      <div className="row">
        <div className="col-6 post_picture__row ">
          <img alt='gem-post' src={this.props.images[0]} className="position__image_three"/>
          <img alt='gem-post' src={this.props.images[1]} className="position__image_three"/>
        </div>

        <div className="col-6 post_picture__row">
          <img alt='gem-post' src={this.props.images[2]} className="position__image__5" />
        </div>
      </div>
    )
  }
}
import React from 'react';


import './position.scss';

export default class PositionSecond extends React.Component {
  render() {
    return (
      <div className="row">
      	<div className="col-6 post_pictury__row">
      		<img alt='gem-post' src={this.props.images[0]} className="position__image_two" />
      	</div>

      	<div className="col-6 post_pictury__row">
      		<img alt='gem-post' src={this.props.images[1]} className="position__image_two" />
      	</div>
      </div>
    )
  }
}
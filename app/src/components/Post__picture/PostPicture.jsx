import React from 'react';

import PositionFirst from './Position/PositionFirst.jsx';
import PositionSecond from './Position/PositionSecond.jsx';
import PositionThird from './Position/PositionThird.jsx';
import PositionFourth from './Position/PositionFourth.jsx';
import PositionFive from './Position/PositionFive.jsx';
import PositionSix from './Position/PositionSix.jsx';
import PositionMobile from './Position/PositionMobile.jsx';

export default class PostPicture extends React.Component {
  render() {
    let picture;
    const countImages = this.props.images.length;
    if (countImages === 1) picture = (<PositionFirst images={this.props.images} />);
    else if (document.body.clientWidth < 992 && countImages > 0) picture = (<PositionMobile images={this.props.images} />);
    else if (countImages === 2) picture = (<PositionSecond images={this.props.images} />);
    else if (countImages === 3) picture = (<PositionThird images={this.props.images} />);
    else if (countImages === 4) picture = (<PositionFourth images={this.props.images} />);
    else if (countImages === 5) picture = (<PositionFive images={this.props.images} />);
    else if (countImages > 5) picture = (<PositionSix images={this.props.images} length={this.props.images.length} />);
    else picture = (<div/>);

    return (
      <div className="Pictury__form">{picture}</div>
    )}
}
import React from 'react';
import {connect} from "react-redux";
import MoneyHelp from '../MoneyHelp';
import IndexPromo from '../IndexPromo';
import IndexCommunication from '../IndexCommunication';
import NewsPreview from '../NewsPreview';
import Button from '../Button';
import Sponsor from '../Sponsor';
import OwlCarousel from 'react-owl-carousel2';
import IndexNewsBlock from  '../IndexNewsBlock';

import '../../Styles/index.scss';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class Index extends React.Component {
	constructor() {
    super(...arguments);
    this.state = {
      Loaded: false,
      News: [],
      MoneyHelp:[],
      error:"",
    };
  }

  componentWillUnmount() {
    this.props.onClose();
  }

  componentWillMount() {
    this.props.onLoad();
    if (!this.state.Loaded) {
      fetch(`${process.env.API_URL}/news/preview?count=3`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      }).then((response) => {
        response.json().then((data) => {
          let messages = [];
          if (data.message.type === "error") {
            this.setState({Loaded: true, error: data.message.message});
            return;
          }

          let months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Ноябрь', 'Декабрь'];
          if (response.status !== 412) {
            messages = data.data.map(msg => {
              return {
              	day: new Date(msg.date).getDate(),
              	date: String(months[new Date(msg.date).getMonth()+1])+" "+String(new Date(msg.date).getFullYear()),
              	title: msg.title,
              	text: msg.text,
              	image: msg.img,
              	link : msg.link,
              };
            });
          }
          this.setState({Loaded: true, News: messages, errorText: data.message.message});
        });
      }).catch((response) => {});

       fetch(`${process.env.API_URL}/money-help?count=10`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      }).then((response) => {
        response.json().then((data) => {
          let money = [];
          if(data.message.message.type === "error")
            return;
          if (response.status !== 412) {
            money = data.data.map(msg => {
              return {
                image: msg.img,
                remain: msg.remain,
                total: msg.total,
                title: msg.title,
                percent: msg.readyPercent,
                id: msg._id
              };
            });
          }
          this.setState({MoneyHelp: money});
        });
      }).catch((response) => {});
    }
  }


  render() {
		const options = {
      loop: false,
      margin: 45,
      items: 3,
      nav: true,
      responsive:{
        0: {
          items: 1
        },

        980: {
          items: 2
        },

        1250: {
          items: 3
        },
      }
    };
		const helpMoney = (
      <OwlCarousel options={options} className="MoneyHelp__carusel">
        { !this.state.Loaded ? (<h4>Загрузка...</h4>) : this.state.Loaded && this.state.MoneyHelp.length > 0 ?
          this.state.MoneyHelp.map(item => ((
            <div key={item.id} className={"item"}><MoneyHelp className='index-money' img={item.image} title={item.title} total={item.total} remained={item.remain} percent={item.percent} /></div>
          ))) :
          (<h4>Нет </h4>)
        }
      </OwlCarousel>
    );

		const helpMoneyTitle = (
      <div className="index__money__header">
        <h1	className="index__money__header__header">Помогаем собрать деньги на лечение</h1>
        <p className="index__money__header__text">
          площадка для обмена информацией, взаимопомощи<br/>
          проведения консультаций между больными,<br/>
          родителями и врачами по всей России
        </p>
      </div>
    );

    return (
      <div className="container">
        <IndexPromo />
        <IndexCommunication/>

        <div className="index__news">
          <h1 className="index__news__header">Новости</h1>
          <p className="index__news__text">события, анонсы мероприятий и наши отчеты</p>
          <div className="news__container d-md-block d-none">
            { !this.state.Loaded ? (<h4>Загрузка...</h4>) : this.state.Loaded && this.state.News.length > 0 ?
              this.state.News.map(item => ((
                <NewsPreview key={item.link} day={item.day} date={item.date} title={item.title} text={item.text} image={item.image} link={item.link} />))) :
              (<h4>Нет новостей</h4>)
            }
          </div>
            <div className="news__container__min d-block d-xs-block d-sm-block d-md-none d-lg-none">
              { !this.state.Loaded ? (<h4>Загрузка...</h4>) : this.state.Loaded && this.state.News.length > 0 ?
                this.state.News.map(item => ((
                  <IndexNewsBlock key={item.link} image={item.image} title={item.title} date={item.day+" "+item.date} link={item.link}  />))) :
                (<h4>Нет новостей</h4>)
              }
            </div>
            <div className="Buttons__news">
              <Button onClick={() => { window.location='./news'}} className="button--rounded button--primary news__show-all" text={"ВСЕ НОВОСТИ"} />
            </div>
        </div>

        <h2 className="index__help__header">При поддержке</h2>
        <Sponsor isEggVisible={true} className="index__help__block"/>

      </div>

    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.hideMenu());
    },

    onClose() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(Index);

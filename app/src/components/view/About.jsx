//Model
import React, {Component} from 'react';
import {Tab, TabPanel, TabList} from 'react-web-tabs';
import {connect} from "react-redux";
import TabControl from '../TabControl';
import Card from '../Card';
import Col from '../Col';
import Sponsor from '../Sponsor';


import sendimage from '../../img/profile-tip/mail-send.png';
//Styles
import '../../Styles/About.scss';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class About extends Component {
  componentWillMount() {
    this.props.onLoad();
  }

  componentWillUnmount() {
    this.props.onClose();
  }
  //onChange={(tabId) => { console.log(tabId) }}
  render() {
    return (
    <div className="container About">
      <div className="About__header">
        <h1 className="About__header__text">О проекте</h1>
      </div>

      <TabControl defaultTabID="one" className={"AboutTabList"}>
        <TabList>
          <Tab tabFor="one">Общая информация</Tab>
        </TabList>

        <TabPanel tabId="one">
          <div className="row">
            <Col lg="7" md="12" sm="12" className="About__all__left">
              <p className="About__all__left__text">Проект <a className="About__all__left__href_to_home" href="/"> gemonet.ru </a>среда для общения, объединения и кооперации больных гемофилией и их родственников по всей стране.</p>
              <p className="About__all__left__text"><b className="About__all__left__header">Гемофилия </b> — редкое наследственное заболевание, связанное с нарушением коагуляции (процессом свёртывания крови). При этом заболевании возникают кровоизлияния в суставы, мышцы и внутренние органы, как спонтанные, так и в результате травмы или хирургического вмешательства. При гемофилии резко возрастает опасность гибели пациента от кровоизлияния в мозг и другие жизненно важные органы, даже при незначительной травме. Больные с тяжёлой формой гемофилии подвергаются инвалидизации вследствие частых кровоизлияний в суставы (гемартрозы) и мышечные ткани (гематомы).</p>
              <p className="About__all__left__text">Гемофилия появляется из-за изменения одного гена в хромосоме X. Различают три типа гемофилии (A, B, C).</p>
              <p className="About__all__left__text"><b className="About__all__left__header">Гемофилия A </b> (рецессивная мутация в X-хромосоме) вызывает недостаточность в крови необходимого белка — так называемого фактора VIII (антигемофильного глобулина). Такая гемофилия считается классической, она встречается наиболее часто, у 80—85 % больных гемофилией. Тяжёлые кровотечения при травмах и операциях наблюдаются при уровне VIII фактора — 5—20 %.</p>
              <p className="About__all__left__text"><b className="About__all__left__header">Гемофилия B </b> (рецессивная мутация в X-хромосоме) — недостаточность фактора плазмы IX (Кристмаса). Нарушено образование вторичной коагуляционной пробки.</p>
              <p className="About__all__left__text"><b className="About__all__left__header">Гемофилия C </b> (аутосомный рецессивный либо доминантный (с неполной пенетрантностью) тип наследования, то есть встречается как у мужчин, так и у женщин) — недостаточность фактора крови XI, известна в основном у евреев-ашкеназов. В настоящее время гемофилия С исключена из классификации, так как её клинические проявления значительно отличаются от А и В.</p>
              <p className="About__all__left__text"></p>
            </Col>

            <Col lg="1" className="d-lg-block d-none"/>

            <Col lg="4" md="4" className="About__all__right d-lg-block d-none">
              <Card cardShadow='no-shadow' className="About__all__right__card">
                <h1 className="About__all__right__header">Цели проекта</h1>
                <ul className="About__all__right__list">
                  <li className="About__all__right__list__item">объединить сообщество больных для обмена опытом;</li>
                  <li className="About__all__right__list__item">повысить качество жизни больных гемофилией;</li>
                  <li className="About__all__right__list__item">собрать <a className="About__all__left__href_to_home" href="/wiki"> базу знаний</a>;</li>
                  <li className="About__all__right__list__item">дать возможность больным искать спонсоров для покупки медикаментов и проведения платных операций по трансплантации суставов;</li>
                  <li className="About__all__right__list__item">получение online консультации врачей гематологов, удаленного проведенное проведение экстренных видео-совещаний;</li>
                  <li className="About__all__right__list__item">создать возможности для трудоустройства и реализации в жизни.</li>
                </ul>
              </Card>
            </Col>
          </div>
          <div className="About__all__block_sponsor">
            <h1 className="About__all__block_sponsor__header">При поддержке</h1>
            <Sponsor/>
          </div>
        </TabPanel>
      </TabControl>
    </div>
    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.hideMenu());
    },

    onClose() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(About);

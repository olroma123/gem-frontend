import React from 'react';
import { connect } from 'react-redux';
import Card from '../Card';
import Col from '../Col';
import Row from '../Row';
import Shapes from '../Shapes';
import RegistrationForm from '../RegistrationForm';
import '../../Styles/regestration.scss';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class Registration extends React.Component {
  componentWillMount() {
    this.props.onLoad();
  }

  componentWillUnmount() {
    this.props.onClose();
  }

  render() {
    return (
      <div style={{height: '100%'}}>
        <Row style={{height: '100%', overflow: 'hidden'}}>
          <Shapes />
          <Col xs="1" md="3"/>
          <Col className="registration__column" xs="10" md="6">
            <Card className="registration__card">
              <Row style={{height: '100%'}}>
                <Col className="registration__top" xs="12">
                  <h4 className="registration__title">Регистрация</h4>
                  <RegistrationForm />
                </Col>
              </Row>
            </Card>
          </Col>
          <Col xs="1" md="3"/>
        </Row>
      </div>
    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.hideMenu());
    },

    onClose() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(Registration);

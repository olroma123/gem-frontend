import React, {Component} from 'react';
import {connect} from 'react-redux';
import Card from '../Card';
import VacancyId from '../VacancyId';
import CardEmptyTitle from '../Card/CardEmptyTitle';
import {Tab, TabPanel, TabList} from 'react-web-tabs';
import TabControl from '../TabControl';
import UserItem from '../Admin/UserItem';
import "../../Styles/admin.scss";
import ForumStructure from '../Admin/ForumConsultation';
import DocumentTrans from "../DocumentTrans/DocumentTrans";
import Button from "../Button/Button";
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class AdminPage extends Component {
  state = {
    confirmed: [],
    Loaded: false,
    needConfirm: [],
    Text: "",
    Document: [],
    vacancy: []
  };

  update = () => {
    // Validate role by access to admin panel
    fetch(`${process.env.API_URL}/security/${this.props.user.info._id}/admin`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status !== 200) window.location.href = '/home';
    }).catch(err => {
      throw new Error(err);
    });

    // Getting user's that fully confirmed
    fetch(`${process.env.API_URL}/admin/${this.props.user.info._id}/users/confirmed`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      response.json().then((data) => {
        this.setState({confirmed: data.data, Loaded: true});
      });
    }).catch(err => {
      //throw new Error(err);
    });

    // Getting user's that need to confirm
    fetch(`${process.env.API_URL}/admin/${this.props.user.info._id}/users/needConfirm`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      response.json().then((data) => {
        this.setState({needConfirm: data.data, Loaded: true});
      });
    }).catch(err => {
      throw new Error(err);
    });

    // Getting user's document confirm
    fetch(`${process.env.API_URL}/admin/${this.props.user.info._id}/documents?status=unverified`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      response.json().then((data) => {
        this.setState({Document: data.data});
      });
    }).catch(err => {
      throw new Error(err);
    });

    // Getting moderation vacancy
    fetch(`${process.env.API_URL}/jobs/moderation/${this.props.user.info._id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 404 || response.status === 500) {
        console.log('Сервер не отвечает');
        return;
      }

      if (response.status === 204) {
        this.setState({vacancy: []});
        return;
      }
      if(response.status === 204)
      {
        return;
      }
      response.json().then((data) => {
        if (response.status === 200) this.setState({vacancy: data.data});
      });
    }).catch(err => {
      throw new Error(err);
    });
  };

  componentWillMount() {
    if (!this.props.user.authorized) {
      window.location.href = '/login';
      return;
    }
    this.update();
  }

  render() {
    const acceptVacancy = (id) => {
      fetch(`${process.env.API_URL}/job/${this.props.user.info._id}?id=${id}`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      }).then(response => {
        if (response.status === 404 || response.status ===  500) console.log('Сервер не отвечает');
        else if (response.status === 200) this.update();
      }).catch(err => {
        console.log(err);
      });
    };

    const deleteVacancy = (id) => {
      fetch(`${process.env.API_URL}/job/${this.props.user.info._id}?id=${id}`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      }).then(response => {
        if (response.status === 404 || response.status === 500) console.log('Сервер не отвечает');
        else if (response.status === 200) this.update();
      }).catch(err => {
        console.log(err);
      });
    };
    
    return (
      <div className='admin'>
        <Card cardShadow='no-shadow' className={'admin__card'}>
          <TabControl className={"AdminTabList"} >
            <TabList>
              <Tab tabFor="users">Пользователи</Tab>
              <Tab tabFor="documents">Документы</Tab>
              <Tab tabFor="vacancy">Вакансии</Tab>
              <Tab tabFor="forum">Форум</Tab>
            </TabList>

            <TabPanel tabId="users">
              <div className={"admin__card__panel"}>
                <TabControl noState={true} defaultTab="User" className={"AboutTabList tab2"} >
                  <TabList aria-orientation={"vertical"}>
                    <Tab tabFor="User">Подтверждённые пользователи</Tab>
                    <Tab tabFor="Pass">Подтверждение пользователей</Tab>
                  </TabList>

                  <TabPanel tabId="User">
                    { !this.state.Loaded ? (<CardEmptyTitle>Загрузка...</CardEmptyTitle>) : this.state.Loaded && this.state.confirmed.length > 0 ?
                      this.state.confirmed.map(item => ((<UserItem actionHandler={this.update} activit={true} key={item.id} userid={item.id} image={item.avatar} name={item.fullName} type={item.role} isConfirmed={true} isDocument={false} />))) :
                      (<CardEmptyTitle>Нет пользователей на модерацию</CardEmptyTitle>)
                    }
                  </TabPanel>

                  <TabPanel tabId="Pass">
                    { !this.state.Loaded ? (<CardEmptyTitle>Загрузка...</CardEmptyTitle>) : this.state.Loaded && this.state.needConfirm.length > 0 ?
                      this.state.needConfirm.map(item => ((
                        <UserItem actionHandler={this.update} key={item.id} userid={item.id} activit={false} document={item.documents} image={item.avatar} name={item.fullName} type={item.role} isConfirmed={false} isDocument={false} />))) :
                      (<CardEmptyTitle>Нет пользователей на модерацию</CardEmptyTitle>)
                    }
                  </TabPanel>
                </TabControl>
              </div>
            </TabPanel>

            <TabPanel tabId="documents">
              <div className={"admin__card__panel"}>
                { this.state.Document.length > 0 ?
                  this.state.Document.map(item => ((<DocumentTrans key={item.id} name={item.fullName} index={item.id} image={item.avatar} document={item.medicalDocuments } />))):
                    <CardEmptyTitle>Нет документов для подтверждения</CardEmptyTitle>
                }
              </div>
            </TabPanel>

            <TabPanel tabId="vacancy">
              <div className={"admin__card__panel"}>
                { this.state.vacancy.length > 0 ?
                  this.state.vacancy.map((item, index) =>
                    ((<VacancyId key={index} onAccepted={() => {acceptVacancy(item._id)}} onDeleted={() => {deleteVacancy(item._id)}} userInfo={item.userInfo} userID={item.userID} isModeration={true} header={item.title} price={item.price} person={item.company} text={item.text} city={item.city} adr={item.address} exp={item.exp} phone={item.contacts}/>))):
                  <CardEmptyTitle>Нет вакансий на модерацию</CardEmptyTitle>
                }
              </div>
            </TabPanel>


            <TabPanel tabId="forum">
              <CardEmptyTitle>Структура разделов форума</CardEmptyTitle>
              <ForumStructure className='forum__structure'/>
            </TabPanel>
          </TabControl>
        </Card>
      </div>
    );
  }
}

export default connect(
  state => ({
    menuState: state.slideMenu,
    user: state.user
  }),

  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    },
  })
)(AdminPage);

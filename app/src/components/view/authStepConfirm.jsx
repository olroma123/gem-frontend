import React, {Component} from 'react';
import {connect} from 'react-redux';
import Shapes from '../Shapes';
import Card from '../Card';
import Col from '../Col';
import Row from '../Row';
import Button from '../Button';
import PDF from '../PDF';
import ProfileDocument from '../ProfileDocument';
import AddProfileDocument from '../AddProfileDocument';
import '../../Styles/authConfirm.scss';
const SlideMenuAction = require('../../actions/SlidemenuAction.js');
const UserAction = require('../../actions/UserAction.js');

class AuthStepConfirm extends Component {
  state = {
    documents: [],
    isAPILoaded: false
  };

  componentWillMount() {
    /*if (this.props.user.authorized) {
      window.location.href = '/home';
      return;
    }*/

    this.props.onLoad();
    this.updateDocuments();
  }

  updateDocuments = () => {
    // Getting auth status for verification update
    this.props.onAuthorize(this.props.user.info._id);

    // Getting all user's documents
    fetch(`${process.env.API_URL}/user/${this.props.user.info._id}/document`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({documents: 'Загрузка...'});
        return;
      }

      response.json().then(data => {
        this.setState({documents: response.status === 200 ? data.data : data.message.message, isAPILoaded: true});
      })
    }).catch(err => {
      console.log(err);
    });
  };

  render() {
    let documents;
    if (typeof this.state.documents !== 'string' && this.state.documents.length > 0)
      documents = this.state.documents.map(item => {
        return (
          <ProfileDocument key={item.id} className={"profile-edit__documents profile-edit__documents--" + item.status}>
            {item.img.includes('pdf') ? <PDF/> : <img alt="gem-document" src={item.img}/>}
          </ProfileDocument>);
      });

    return (
      <div style={{height: '100%'}}>
        <Row style={{height: '100%', overflow: 'hidden'}}>
          <Shapes/>
          <Col xs="1" md="3"/>
          <Col className="auth-confirm__column" xs="10" md="6">
            <Card className="auth-confirm__card">
              <Row style={{height: '100%', width:'100%', marginLeft:'0px'}}>
                <Col xs="12">
                  <h4 className="auth-confirm__title">Загрузка документов</h4>
                  <div className="auth-confirm__description">
                    Для того, чтобы администратор смог вас подвердить, вы должны предоставить медицинские документы,
                    которые подтверждают вашу болезнь. После того, как администратор верифицирует все документы,
                    вам будет открыт доступ в аккаунт
                    <p>Допустимые расширения файла для загрузки: jpeg, jpg, png, pdf </p>
                  </div>

                  <div className="auth-confirm__documents">
                    {documents}
                    <AddProfileDocument addDocumentHandler={this.updateDocuments}/>
                  </div>
                  <p className={"auth-confirm__documents__text"}>{this.state.documents.length>0?"Ваш акаунт ещё не подтверждён. Документы находятся на рассмотрении у администрации сервиса.":"Загрузите фотографии для подтверждения вашей записи"}</p>
                  <Button onClick={this.updateDocuments} text='Обновить' className='button--primary auth-confirm__update'/>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col xs="1" md="3"/>
        </Row>
      </div>
    );
  }
}

export default connect(
  state => ({
    user: state.user,
    menuState: state.slideMenu
  }),
  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.hideMenu());
    },

    onClose() {
      dispatch(SlideMenuAction.default.unhideMenu());
    },

    onAuthorize(token) {
      dispatch(UserAction.default.authorizeWithToken(token));
    }
  })
)(AuthStepConfirm);

import React, { Component } from "react";
import { connect } from 'react-redux';
import ProfilePicture from '../ProfilePicture';
import ProfileInformation from '../ProfileInformation';
import TabControlHome from '../TabControlHome';
import PostForm from '../PostForm';
import CardEmptyTitle from '../Card/CardEmptyTitle';
import {Tab, TabPanel, TabList} from 'react-web-tabs';
import Container from '../Container';
import Row from '../Row';
import Col from '../Col';
import Entry from '../Entry';
import moment from 'moment';
import "../../Styles/home.scss";
moment.suppressDeprecationWarnings = true;
const SlideMenuAction = require('../../actions/SlidemenuAction.js');

class UserPage extends Component {
  state = {
    reminds: [],
    isAPILoaded: false,
    News:[],
    posts: [],
    user: {}
  };

  componentDidMount() {
    if (!this.props.user.authorized && (!this.props.user.info || !this.props.user.info._id)) {
      window.location.replace(`${process.env.URL_FOLDER}login`);
      return;
    } else if (!this.props.user.authorized && this.props.user.info && this.props.user.info._id) {
      window.location.replace(`${process.env.URL_FOLDER}auth-confirm-step`);
      return;
    }
    this.props.onLoad();

    // get User Info
    fetch(`${process.env.API_URL}/user/public?userID=${this.props.match.params.id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({user: '', isAPILoaded: true});
        return;
      }

      if (response.status === 403) {
        window.location.href = '/home';
        return;
      }

      response.json().then(data => {
        let userInfo = response.status === 200 ? data.data : data.message.message;
        this.setState({user: userInfo, isAPILoaded: true})
      }).catch(err => {

      })
    }).catch(err => {

    });

    // Get user posts
    fetch(`${process.env.API_URL}/post/${this.props.match.params.id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status === 500 || response.status === 404) {
        this.setState({posts: '', isAPILoaded: true});
        return;
      }

      response.json().then(data => {
        let posts = response.status === 200 ? data.data : data.message.message;
        this.setState({posts: posts, isAPILoaded: true})
      }).catch(err => {

      })
    }).catch(err => {

    });
  }

  render() {
    let posts, avatar = '/img/default-avatar.jpg';
    if (!this.state.isAPILoaded) posts = (<CardEmptyTitle>Загрузка</CardEmptyTitle>);
    else if (this.state.isAPILoaded && Array.isArray(this.state.posts.posts) && this.state.posts.posts.length > 0) {
      posts = this.state.posts.posts.map(item => {
        let diff = moment(new Date()).diff(moment(new Date(item.date)), 'days');
        let date = moment(new Date(item.date)).format('DD.MM.YYYY');
        return (<Entry key={item.date}
                       name={this.state.posts.user.fullName}
                       img={this.state.posts.user.avatar}
                       image={item.images}
                       date={diff < 2 ? moment(new Date().fromNow(new Date(item.date))).fromNow() : date}
                       post={item.comments}
                       id={item.id}
                       text={item.text}
                       Myentry={false}
                       isSticky={item.isSticky || false}
        />)
        }
      );
      avatar = this.state.posts.user.avatar;
    }
    else if (this.state.isAPILoaded) posts = (<CardEmptyTitle>Нет постов</CardEmptyTitle>);

    if (this.state.isAPILoaded)
      avatar = this.state.user.avatar;

    return (
      <Container style={{marginTop: '25px'}}>
        <Row>
          <Col style={{padding: '0'}} xs="12" lg="8">
            <ProfileInformation avatar={avatar} userID={this.props.match.params.id}/>
          </Col>

          <Col style={{paddingRight: '0'}} className="d-none d-lg-flex" xs="0" lg="4">
            <ProfilePicture avatar={avatar} isEditable={false} userID={this.props.match.params.id} className="profile__picture-block" />
          </Col>
        </Row>

        <Row>
          <Col style={{paddingLeft: '3px'}} xs="12" lg="8" className="home">
            <TabControlHome defaultTabID="one" >
              <TabList>
                <Tab tabFor="one">Стена пользователя</Tab>
              </TabList>

              <TabPanel tabId="one">
                <PostForm>{posts}</PostForm>
              </TabPanel>
            </TabControlHome>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),

  dispatch => ({
    onLoad() {
      dispatch(SlideMenuAction.default.unhideMenu());
    }
  })
)(UserPage);

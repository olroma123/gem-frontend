import React, { Component } from 'react';
import './DropdownSimple.scss';

export default class DropdownSimple extends Component {
  state = {
    value: ''
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedItem) {
      let index = this.props.items.findIndex(item => item === nextProps.selectedItem);
      this.setState({value: nextProps.items[index]});
    }
  }

  _onChanged = (e) => {
    this.setState({value: e.target.value});
    if (this.props.onChange) this.props.onChange();
  };

  render() {
    return (
      <select id={this.props.id} value={this.state.value} onChange={this._onChanged} className={'dropdown-simple ' + (this.props.className || '')}>
        {this.props.items.map((item, index) => ((<option key={index} value={item}>{item}</option>)))}
      </select>
    );
  }
}

import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Route, BrowserRouter, Switch} from "react-router-dom";

// ===================== PAGES ====================
import Forumlink from './components/view/forums';
import topics from './components/view/topics';
import Index from './components/view/index';
import Home from './components/view/home';
import NewsAdd from './components/view/NewsAdd';
import UserPage from './components/view/user';
import Dialogs from './components/view/dialogs';
import Login from './components/view/login';
import Forum from  './components/view/forum';
import ForumCreateTopic from  './components/view/forumAddTopic';
import AuthStepConfirm from './components/view/authStepConfirm';
import Registration from './components/view/registration';
import ForgotPassword from './components/view/ForgotPassword';
import resetpassword from './components/view/resetpassword';
import ProfileSetting from './components/view/profileSetting';
import Treatment from './components/view/Treatment';
import About from './components/view/About';
import Dialog from './components/view/dialog';
import Friends from './components/view/friends';
import TreatmentPlan from './components/view/TreatmentPlan';
import news from './components/view/news';
import Error500 from './components/view/Error500.jsx';
import EmptyPage from './components/view/empty';
import qa from './components/view/qa';
import Question from './components/view/question';
import QuestionDetails from './components/Consultation/Question';
import QuestionAdd from './components/view/question-add';
import Wiki from './components/view/wiki';
import Donate from './components/view/donate';
import NewsPage from  './components/view/NewsPage';
import Jobs from  './components/view/Jobs';
import Vacancy from  './components/view/Vacancy';
import Admin from './components/view/admin';
import Summary from './components/view/summary';
import SummaryProfile from './components/view/summaryProfile';
import VacancyAddPage from './components/view/VacancyAdd';
import Help from './components/view/userHelp';
// ================================================

// ==================== COMPONENTS ================
// Bootstrap
import Col from './components/Col';
import Container from './components/Container';
import Row from './components/Row';
// Container's or Item's
import Card from './components/Card';
import GemoLogo from './components/GemoLogo';
import Footer from './components/Footer';

// Layout
import Header from './components/Header';
import HeaderSearch from './components/HeaderSearch';
import NavItem from './components/NavItem';
import SlideMenu from './components/SlideMenu';
// ================================================

// ====================== STYLES ==================
import './App.scss';
import './Styles/common/_base.scss';
import shapeLeft from './img/shapes/shape-left-bottom.svg';
import shapeCircle from './img/shapes/shape-circle.svg';
import shapeO from './img/shapes/shape-o.svg';
// ================================================
const UserAction = require('./actions/UserAction.js');
const NotificationAction = require('./actions/NotificationAction.js');

class App extends Component {
  constructor() {
    super(...arguments);

    this.state = {
      clicks : false
    }

    this.clicksearch = this.clicksearch.bind(this);
  }
  componentDidMount() {
    this.props.onValidateUser(this.props.user);
    this.props.onNotificationInit(this.props.user);
    if (window.socket)
      window.socket.on('notification', this.props.onNotificationUpdate);
  }

  componentWillUnmount() {
  }

  clicksearch()
  {
    if(this.state.clicks)
      this.setState({clicks : false});
    else
      this.setState({clicks : true});

  }

  render() {
    const shouldRenderSlideMenu = () => {
      return this.props.user.authorized &&
        !window.location.href.includes('login') &&
        !window.location.href.includes('About') &&
        !window.location.href.includes('Error500') &&
        !window.location.href.includes('about') &&
        !window.location.href.includes('wiki') &&
        !window.location.href.includes('index') && window.location.href !== `http://${window.location.host}${process.env.URL_FOLDER}`
    };

    const isAuthPage = () => {
      return window.location.href.includes('login') ||
        window.location.href.includes('auth-confirm-step') ||
        window.location.href.includes('registration') ||
        window.location.href.includes('resetPassword') ||
        window.location.href.includes('ForgotPassword')
    };

    const searchBlock = (<HeaderSearch className="d-none d-md-inline"/>);

    let slideMenu, contentColumn = 12;
    if (shouldRenderSlideMenu() && !isAuthPage()) {
      slideMenu = ( <SlideMenu /> );
      contentColumn = 9;
    }


    let contentMaxWidth;
    if (isAuthPage())
      contentMaxWidth = 'initial';

    let shapes;
    if (window.location.href.includes('index') || window.location.href === `http://${window.location.host}${process.env.URL_FOLDER}`) {
      shapes = (
        <div className="promo__shapes">
          <img src={shapeLeft} alt="gem-shape"/>
          <img src={shapeCircle} alt="gem-shape"/>
          <img src={shapeO} alt="gem-shape"/>
        </div>
      );
    }
    return (
      <BrowserRouter>
        <div className="App">
          <Container style={{minHeight: isAuthPage() ? '0' : '100%', height: isAuthPage() ? 'calc(100% - 54px)' : 'initial'}} fluid={true} className="wrapper">
            <Row className="Header__index">
              <Col style={{padding: '0'}} xs="12">
                <Card className="header__card">
                  <Container>
                    <Row>
                      <Col lg="3" md="12">
                        <GemoLogo redirect='/' isPrimary={false}/>
                      </Col>

                      <Col lg="9" md="12">
                        <Header>
                          <NavItem isActive={true} isInline={true} link={`${process.env.URL_FOLDER}about`} text="О проекте" />
                          <NavItem isInline={true} link={`${process.env.URL_FOLDER}wiki`} text="База знаний" />
                          <NavItem isInline={true} link={`${process.env.URL_FOLDER}questions`} text="Консультации" />
                          <NavItem isInline={true} link={`${process.env.URL_FOLDER}news`} text="Новости" />
                          <NavItem isInline={true} link={`${process.env.URL_FOLDER}jobs`} dot={true} text="Вакансии" />
                          <NavItem className={this.props.user.authorized ? 'd-none' : 'd-md-none'} isInline={true} link={`${process.env.URL_FOLDER}login`} text="Вход" />
                          <NavItem className={this.props.user.authorized ? 'd-md-none' : 'd-none'} isInline={true} link={`${process.env.URL_FOLDER}home`} text="Профиль" />
                        </Header>
                      </Col>
                    </Row>
                  </Container>
                </Card>
              </Col>
            </Row>

            <Row id='content-app' style={{height: '100%'}}>
              {shapes}
              <Container style={{maxWidth: contentMaxWidth}}>
                <Row style={{height: '100%'}}>
                  {slideMenu}
                  <Col style={{height: '100%'}} className="content" xs="10" lg={contentColumn.toString()}>
                    <Switch>
                      <Route path={`${process.env.URL_FOLDER}forum/:id`} component={Forumlink}/>
                      <Route path={`${process.env.URL_FOLDER}forum-topic-add/:themeId`} component={ForumCreateTopic}/>
                      <Route path={`${process.env.URL_FOLDER}home`} component={Home}/>
                      <Route path={`${process.env.URL_FOLDER}id/:id`} component={UserPage}/>
                      <Route path={`${process.env.URL_FOLDER}dialogs`} component={Dialogs}/>
                      <Route path={`${process.env.URL_FOLDER}login`} component={Login}/>
                      <Route path={`${process.env.URL_FOLDER}auth-confirm-step`} component={AuthStepConfirm}/>
                      <Route path={`${process.env.URL_FOLDER}registration`} component={Registration}/>
                      <Route path={`${process.env.URL_FOLDER}forgotPassword`} component={ForgotPassword}/>
                      <Route path={`${process.env.URL_FOLDER}resetPassword/:id`} component={resetpassword}/>
                      <Route path={`${process.env.URL_FOLDER}treatment`} component={Treatment}/>
                      <Route path={`${process.env.URL_FOLDER}treatment-plan`} component={TreatmentPlan}/>
                      <Route path={`${process.env.URL_FOLDER}profileEdit`} component={ProfileSetting}/>
                      <Route path={`${process.env.URL_FOLDER}about`} component={About}/>
                      <Route path={`${process.env.URL_FOLDER}dialog`} component={Dialog}/>
                      <Route path={`${process.env.URL_FOLDER}friends`} component={Friends}/>
                      <Route path={`${process.env.URL_FOLDER}news/:id`}  component={news}/>
                      <Route path={`${process.env.URL_FOLDER}index`} exact component={Index}/>
                      <Route path={`${process.env.URL_FOLDER}`} exact component={Index}/>
                      <Route path={`${process.env.URL_FOLDER}qa`} exact component={qa}/>
                      <Route path={`${process.env.URL_FOLDER}questions`} component={Question}/>
                      <Route path={`${process.env.URL_FOLDER}question`} component={QuestionDetails}/>
                      <Route path={`${process.env.URL_FOLDER}question-add`} component={QuestionAdd}/>
                      <Route path={`${process.env.URL_FOLDER}wiki`} exact component={Wiki}/>
                      <Route path={`${process.env.URL_FOLDER}help`} exact component={Help}/>
                      <Route path={`${process.env.URL_FOLDER}donate`} exact component={Donate}/>
                      <Route path={`${process.env.URL_FOLDER}`} exact component={Error500}/>
                      <Route path={`${process.env.URL_FOLDER}news`} exact component={NewsPage}/>
                      <Route path={`${process.env.URL_FOLDER}jobs`} exact component={Jobs}/>
                      <Route path={`${process.env.URL_FOLDER}admin`} exact component={Admin}/>
                      <Route path={`${process.env.URL_FOLDER}jobs/:id`} exact component={Vacancy}/>
                      <Route path={`${process.env.URL_FOLDER}summary`} exact component={Summary}/>
                      <Route path={`${process.env.URL_FOLDER}summary-profile/:id`} exact component={SummaryProfile}/>
                      <Route path={`${process.env.URL_FOLDER}vacancy-create`} exact component={VacancyAddPage}/>
                      <Route path={`${process.env.URL_FOLDER}forum`} exact component={Forum}/>
                      <Route path={`${process.env.URL_FOLDER}topic/:id`} exact component={topics}/>
                      <Route path={`${process.env.URL_FOLDER}news-create`} exact component={NewsAdd}/>
                      <Route path='*' exact={true} component={EmptyPage}/>
                    </Switch>
                  </Col>
                </Row>
              </Container>
            </Row>
          </Container>
          <Footer isAuthPage={isAuthPage()}/>
        </div>
      </BrowserRouter>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({
    onValidateUser(user) {
      if (!user.authorized) return;
      dispatch(UserAction.default.validateUser(user.info));
    },

    onNotificationUpdate(data) {
      dispatch(NotificationAction.default.updateNotification(data))
    },

    onNotificationInit(user) {
      if (!user.authorized) return;
      dispatch(NotificationAction.default.getNotification(user))
    }
  }), null, {
    pure: false
  }
)(App);
